//Author: Andrew Baumher
import java.io.*;

public class Main
{
    public static void main(String[] args) throws IOException
    {
        int numFields = 1;
        int width, height;
        BufferedReader in = new BufferedReader(new FileReader("a.txt"));
        String text = in.readLine();
        String[] temp = text.split(" ");
        width = Integer.parseInt(temp[0]);
        height = Integer.parseInt(temp[1]);
        while (width != 0 && height != 0)
        {
            temp = new String[height];
            System.out.println("Field #" + numFields);
            char[][] mineField = new char[width][height];
            for (int y = 0; y < height; y++)
            {
                temp[y] = in.readLine();
            }
            for (int y = 0; y < height; y++)
            {
                for (int x = 0; x < width; x++)
                {
                    if (temp[y].charAt(x) == '*')
                    {
                        mineField[x][y] = '*';
                    }
                    else
                    {
                        mineField[x][y] = numBombs(x, y, width, height, temp);
                    }
                }
            }
            display(mineField);
            text = in.readLine();
            temp = text.split(" ");
            width = Integer.parseInt(temp[0]);
            height = Integer.parseInt(temp[1]);
            numFields++;
        }

        in.close();
    }

    static char numBombs(int x, int y, int width, int height, String[] lines)
    {
        int count = 0;
        int xmin, xmax, ymin, ymax;
        if (x > 0)
        {
            xmin = x - 1;
        }
        else
            xmin = x;
        if (x < width - 1)
        {
            xmax = x + 1;
        }
        else
            xmax = x;

        if (y > 0)
        {
            ymin = y - 1;
        }
        else
            ymin = y;
        if (y < width - 1)
        {
            ymax = y + 1;
        }
        else
            ymax = y;

        for (int z = xmin; z <= xmax; z++)
        {
            for (int w = ymin; w <= ymax; w++)
            {
                if (lines[w].charAt(z) == '*')
                {
                    count++;
                }
            }
        }
        String temp = "" + count;
        return temp.charAt(0);
    }

    static void display(char[][] field)
    {
        for (int y = 0; y < field[0].length; y++)
        {
            for (char[] field1 : field) {
                System.out.print(field1[y]);
            }
            System.out.println();
        }
    }
}
